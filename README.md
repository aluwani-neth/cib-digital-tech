# CIB DIGITAL TECH 

# Instruction to build and Run

Insure you have .net core 3.1 or latest install

# In Visual Studio 

* [ ] Restore nuget package if not loading.
* [ ] Set CIBDigitalTech.Infrastructure project as a startup project.
* [ ] In package manager console run "update-database".
* [ ] When above completed set CIBDigitalTech.WebApi project as a startup project.
* [ ] Run the prject, it will start up in your browser with a swagger documentation.


# In Visual Studio Code
* [ ] Select project solution
* [ ] restore packages
* [ ] select CIBDigitalTech.Infrastructure folder in terminal
* [ ] run "dotnet ef database update"
* [ ] When above completed select CIBDigitalTech.WebApi folder in terminal.
* [ ] run "dotnet run"
* [ ] Open your browser and run "https://localhost:5001/swagger"


#Database.

* [ ] This project run in SQL SERVER 
* [ ] You can update the connection string in appSettings.Json file in both CIBDigitalTech.WebApi and CIBDigitalTech.Infrastructure


# Logs
In CIBDigitalTech.WebApi you can change the path of log files.



