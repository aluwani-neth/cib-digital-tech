﻿using CIBDigitalTech.Core.Dtos.Response;
using CIBDigitalTech.Core.Interfaces;

namespace CIBDigitalTech.Core.Dtos.Requests.PhoneBook
{
    public class CreatePhoneBookRequest : IRequest<ServiceResponse>
    {
        public string Name { get; set; }
       
        public CreatePhoneBookRequest(string name)
        {
            Name = name;
        }
    }
}