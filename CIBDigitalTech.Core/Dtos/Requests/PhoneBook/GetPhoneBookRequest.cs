﻿using CIBDigitalTech.Core.Dtos.Responses.PhoneBook;
using CIBDigitalTech.Core.Interfaces;

namespace CIBDigitalTech.Core.Dtos.Requests.PhoneBook
{
    public class GetPhoneBookRequest : IRequest<GetPhoneBookResponse>
    {
    }
}
