﻿using CIBDigitalTech.Core.Dtos.Responses.PhoneBook;
using CIBDigitalTech.Core.Interfaces;
using System;

namespace CIBDigitalTech.Core.Dtos.Requests.PhoneBook
{
    public class GetPhoneBookEntryByNameRequest : IRequest<GetPhoneBookResponse>
    {
        public Guid PhoneBookId { get; set; }
        public string Name { get; set; }

        public GetPhoneBookEntryByNameRequest(Guid phoneBookId, string name)
        {
            PhoneBookId = phoneBookId;
            Name = name;
        }
    }
}
