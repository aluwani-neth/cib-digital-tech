﻿using CIBDigitalTech.Core.Dtos.Response;
using CIBDigitalTech.Core.Interfaces;
using System;

namespace CIBDigitalTech.Core.Dtos.Requests.Entry
{
    public class CreateEntryRequest : IRequest<ServiceResponse>
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public Guid PhoneBookId { get; set; }
        public CreateEntryRequest(string name, string phoneNumber, Guid phoneBookId)
        {
            Name = name;
            PhoneNumber = phoneNumber;
            PhoneBookId = phoneBookId;
        }
    }
}