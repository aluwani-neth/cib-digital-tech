﻿using CIBDigitalTech.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CIBDigitalTech.Core.Dtos.Responses.PhoneBook
{
    public class PhoneBookDto
    {
        public Guid Id { get; set; }
        public string PhoneBookName { get; set; }
        public List<Entry> Entry { get; set; }
    }
}
