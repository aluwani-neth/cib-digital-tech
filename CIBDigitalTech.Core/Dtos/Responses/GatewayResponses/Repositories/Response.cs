﻿using CIBDigitalTech.Core.Dtos.Responses.GatewayResponses;
using System.Collections.Generic;

namespace CIBDigitalTech.Core.Dtos.Response.GatewayResponses.Repositories
{
    public sealed class Response : BaseGatewayResponse
    {
        public string ResponseMessage { get; }
        public Response(string responseMessage, bool success = false) : base(success)
        {
            ResponseMessage = responseMessage;
        }
            
    }
}
