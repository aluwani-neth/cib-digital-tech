﻿using System.Collections.Generic;
using System.Net;

namespace CIBDigitalTech.Core.Dtos.Responses.GatewayResponses
{
  public abstract class BaseGatewayResponse
  {
        public bool Success { get; }

    protected BaseGatewayResponse(bool success=false)
    {
         Success = success;
    }
  }
}

