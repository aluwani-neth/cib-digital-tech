﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CIBDigitalTech.Core.Dtos.Response
{
    public class ServiceResponse : ResponseMessage
    {
        public string ResponseMessage { get; }

    

        public ServiceResponse(int statusCode, bool success = false, string message = null) : base(success, message)
        {
            
        }

        public ServiceResponse(string responseMessage, bool success = false, string message = null) : base(success, message)
        {
            ResponseMessage = responseMessage;
        }
    }
}
