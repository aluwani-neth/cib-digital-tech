﻿using Autofac;
using CIBDigitalTech.Core.Interfaces.Entry;
using CIBDigitalTech.Core.Interfaces.PhoneBook;
using CIBDigitalTech.Core.Services.Entry;
using CIBDigitalTech.Core.Services.PhoneBook;
using System;
using System.Collections.Generic;
using System.Text;

namespace CIBDigitalTech.Core
{
    public class CoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PhoneBookService>().As<ICreatePhoneBook>().InstancePerLifetimeScope();
            builder.RegisterType<PhoneBookService>().As<IGetPhoneBooks>().InstancePerLifetimeScope();
            builder.RegisterType<PhoneBookService>().As<IGetPhoneBookEntryByName>().InstancePerLifetimeScope();
            builder.RegisterType<EntryService>().As<ICreateEntry>().InstancePerLifetimeScope();
        }
    }
}
