﻿using CIBDigitalTech.Core.Dtos.Requests.PhoneBook;
using CIBDigitalTech.Core.Dtos.Response;

namespace CIBDigitalTech.Core.Interfaces.PhoneBook
{
    public interface ICreatePhoneBook : IRequestHandler<CreatePhoneBookRequest, ServiceResponse>
    {
    }
}
