﻿using CIBDigitalTech.Core.Dtos.Requests.PhoneBook;
using CIBDigitalTech.Core.Dtos.Responses.PhoneBook;

namespace CIBDigitalTech.Core.Interfaces.PhoneBook
{
    public interface IGetPhoneBooks : IRequestHandler<GetPhoneBookRequest, GetPhoneBookResponse>
    {
    }
}
