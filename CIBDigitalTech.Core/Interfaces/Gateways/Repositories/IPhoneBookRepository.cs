﻿using CIBDigitalTech.Core.Dtos.Response.GatewayResponses.Repositories;
using CIBDigitalTech.Core.Dtos.Responses.PhoneBook;
using CIBDigitalTech.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CIBDigitalTech.Core.Interfaces.Gateways.Repositories
{
    public interface IPhoneBookRepository : IRepository<Entities.PhoneBook>
    {
        Task<Response> Create(string name);
        Task<IEnumerable<PhoneBookDto>> GetPhoneBooks();
        Task<IEnumerable<PhoneBookDto>> GetPhoneBookEntryByName(Guid phoneId, string name);
    }
}
