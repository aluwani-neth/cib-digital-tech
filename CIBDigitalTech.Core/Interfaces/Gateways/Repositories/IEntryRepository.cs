﻿using CIBDigitalTech.Core.Dtos.Response.GatewayResponses.Repositories;
using System;
using System.Threading.Tasks;

namespace CIBDigitalTech.Core.Interfaces.Gateways.Repositories
{
    public interface IEntryRepository : IRepository<Entities.Entry>
    {
        Task<Response> Create(string name, string phoneNumber, Guid phoneBookId);
    }
}
