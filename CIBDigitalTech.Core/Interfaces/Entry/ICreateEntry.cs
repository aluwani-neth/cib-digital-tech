﻿using CIBDigitalTech.Core.Dtos.Requests.Entry;
using CIBDigitalTech.Core.Dtos.Response;

namespace CIBDigitalTech.Core.Interfaces.Entry
{
    public interface ICreateEntry : IRequestHandler<CreateEntryRequest, ServiceResponse>
    {
    }
}
