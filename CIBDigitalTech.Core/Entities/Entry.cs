﻿using CIBDigitalTech.Core.Shared;
using System;

namespace CIBDigitalTech.Core.Entities
{
    public class Entry : BaseEntity
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public Guid PhoneBookId { get; set; }
        public PhoneBook PhoneBook { get; set; }

        internal Entry() { }
        internal Entry(string name, string phoneNumber, Guid phoneBookId)
        {
            Name = name;
            PhoneNumber = phoneNumber;
            PhoneBookId = phoneBookId;
        }
    }
}

