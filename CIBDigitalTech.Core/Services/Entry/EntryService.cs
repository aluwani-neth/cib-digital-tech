﻿using CIBDigitalTech.Core.Dtos.Requests.Entry;
using CIBDigitalTech.Core.Dtos.Response;
using CIBDigitalTech.Core.Interfaces;
using CIBDigitalTech.Core.Interfaces.Entry;
using CIBDigitalTech.Core.Interfaces.Gateways.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CIBDigitalTech.Core.Services.Entry
{
    public sealed class EntryService : ICreateEntry
    {
        private readonly IEntryRepository _entryRepository;

        public EntryService(IEntryRepository entryRepository)
        {
            _entryRepository = entryRepository;
        }

        public async Task<bool> Handle(CreateEntryRequest message, IOutputPort<ServiceResponse> outputPort)
        {
            var response = await _entryRepository.Create(message.Name, message.PhoneNumber, message.PhoneBookId);
            outputPort.Handle(response.Success ? new ServiceResponse(response.ResponseMessage, true) : new ServiceResponse((int)HttpStatusCode.Created));
            return response.Success;
        }
    }
}
