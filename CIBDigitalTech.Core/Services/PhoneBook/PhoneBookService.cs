﻿using CIBDigitalTech.Core.Dtos.Requests.PhoneBook;
using CIBDigitalTech.Core.Dtos.Response;
using CIBDigitalTech.Core.Dtos.Responses.PhoneBook;
using CIBDigitalTech.Core.Interfaces;
using CIBDigitalTech.Core.Interfaces.Gateways.Repositories;
using CIBDigitalTech.Core.Interfaces.PhoneBook;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CIBDigitalTech.Core.Services.PhoneBook
{
    public sealed class PhoneBookService : ICreatePhoneBook, IGetPhoneBooks, IGetPhoneBookEntryByName
    {
        private readonly IPhoneBookRepository _phoneBookRepository;

        public PhoneBookService(IPhoneBookRepository phoneBookRepository)
        {
            _phoneBookRepository = phoneBookRepository;
        }
        public async Task<bool> Handle(CreatePhoneBookRequest message, IOutputPort<ServiceResponse> outputPort)
        {
            var response = await _phoneBookRepository.Create(message.Name);
            outputPort.Handle(response.Success ? new ServiceResponse(response.ResponseMessage, true) : new ServiceResponse((int)HttpStatusCode.Created));
            return response.Success;
        }

        public async Task<bool> Handle(GetPhoneBookRequest message, IOutputPort<GetPhoneBookResponse> outputPort)
        {
            var response = await _phoneBookRepository.GetPhoneBooks();
            outputPort.Handle(new GetPhoneBookResponse(response, true, null));

            return true;
        }

        public async Task<bool> Handle(GetPhoneBookEntryByNameRequest message, IOutputPort<GetPhoneBookResponse> outputPort)
        {
            var response = await _phoneBookRepository.GetPhoneBookEntryByName(message.PhoneBookId,message.Name);
            outputPort.Handle(new GetPhoneBookResponse(response, true, null));

            return true;
        }
    }
}
