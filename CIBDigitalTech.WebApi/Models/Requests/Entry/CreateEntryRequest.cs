﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CIBDigitalTech.WebApi.Models.Requests.Entry
{
    public class CreateEntryRequest
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public Guid PhoneBookId { get; set; }
    }
}
