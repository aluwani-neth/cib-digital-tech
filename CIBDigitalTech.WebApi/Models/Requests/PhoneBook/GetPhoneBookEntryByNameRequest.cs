﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CIBDigitalTech.WebApi.Models.Requests.PhoneBook
{
    public class GetPhoneBookEntryByNameRequest
    {
        public Guid PhoneBookId { get; set; }
        public string Name { get; set; }
    }
}
