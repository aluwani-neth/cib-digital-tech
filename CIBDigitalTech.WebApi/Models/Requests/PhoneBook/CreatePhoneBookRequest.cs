﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CIBDigitalTech.WebApi.Models.Requests.PhoneBook
{
    public class CreatePhoneBookRequest
    {
        [Required]
        public string Name { get; set; }
    }
}
