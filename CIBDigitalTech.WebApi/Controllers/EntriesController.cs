﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using CIBDigitalTech.Core.Dtos.Requests.Entry;
using CIBDigitalTech.Core.Interfaces.Entry;
using CIBDigitalTech.WebApi.Presenters.Entry;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CIBDigitalTech.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class EntriesController : ControllerBase
    {
        private readonly ICreateEntry _createEntry;
        private readonly EntryPresenter _entryPresenter;
        private readonly ILogger<EntriesController> _logger;

        public EntriesController(ICreateEntry createEntry,
                                     EntryPresenter entryPresenter,
                                      ILogger<EntriesController> logger)
        {
            _createEntry = createEntry;
            _entryPresenter = entryPresenter;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Models.Requests.Entry.CreateEntryRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var serializableModelState = new SerializableError(ModelState);
                    var modelStateJson = JsonConvert.SerializeObject(serializableModelState);
                    _logger.LogError(modelStateJson);
                    return BadRequest(ModelState);
                }
                _logger.LogInformation(string.Format("Create PhoneBook with payload: {0}", JsonConvert.SerializeObject(request)));
                await _createEntry.Handle(new CreateEntryRequest(request.Name, request.PhoneNumber, request.PhoneBookId), _entryPresenter);
                var response = _entryPresenter.ContentResult;
                _logger.LogInformation(string.Format("Response: {0}", JsonConvert.SerializeObject(request)));
                return response;
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new Exception(ex.Message);
            }
        }
    }
}
