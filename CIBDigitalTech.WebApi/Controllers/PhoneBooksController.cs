﻿using System;
using System.Threading.Tasks;
using CIBDigitalTech.Core.Dtos.Requests.PhoneBook;
using CIBDigitalTech.Core.Interfaces.PhoneBook;
using CIBDigitalTech.WebApi.Presenters.PhoneBook;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Text.Json;
using Newtonsoft.Json;

namespace CIBDigitalTech.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhoneBooksController : Controller
    {
        private readonly ICreatePhoneBook _createPhoneBook;
        private readonly IGetPhoneBooks _getPhoneBooks;
        private readonly IGetPhoneBookEntryByName _getPhoneBookEntryByName;
        private readonly ILogger<PhoneBooksController> _logger;
        private readonly PhoneBookPresenter _phoneBookPresenter;

        public PhoneBooksController(ICreatePhoneBook createPhoneBook,
                                     IGetPhoneBooks getPhoneBooks,
                                     IGetPhoneBookEntryByName getPhoneBookEntryByName,
                                     PhoneBookPresenter phoneBookPresenter,
                                      ILogger<PhoneBooksController> logger)
        {
            _createPhoneBook = createPhoneBook;
            _getPhoneBooks = getPhoneBooks;
            _getPhoneBookEntryByName = getPhoneBookEntryByName;
            _phoneBookPresenter = phoneBookPresenter;
            _logger = logger;
        }
            
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            try
            {
                await _getPhoneBooks.Handle(new GetPhoneBookRequest(), _phoneBookPresenter);
                var response = _phoneBookPresenter.ContentResult;
                _logger.LogInformation(string.Format("Successfully get phonebooks: {0}", JsonConvert.SerializeObject(response)));
                return response;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpGet]
        [Route("getPhoneBookByName")]
        public async Task<ActionResult> Get(Guid phoneBookId, string name)
        {
            try
            {
                await _getPhoneBookEntryByName.Handle(new GetPhoneBookEntryByNameRequest(phoneBookId, name), _phoneBookPresenter);
                var response = _phoneBookPresenter.ContentResult;
                _logger.LogInformation(string.Format("Successfully get Phone Book By Name: {0}", JsonConvert.SerializeObject(response)));
                return response;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Models.Requests.PhoneBook.CreatePhoneBookRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var serializableModelState = new SerializableError(ModelState);
                    var modelStateJson = JsonConvert.SerializeObject(serializableModelState);
                    _logger.LogError(modelStateJson);
                    return BadRequest(ModelState);
                }
                _logger.LogInformation(string.Format("Create PhoneBook with payload: {0}", JsonConvert.SerializeObject(request)));
                await _createPhoneBook.Handle(new CreatePhoneBookRequest(request.Name), _phoneBookPresenter);
                var response = _phoneBookPresenter.ContentResult;
                _logger.LogInformation(string.Format("Response: {0}", JsonConvert.SerializeObject(request)));
                return response;
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new Exception(ex.Message);
            }
        }
    }
}