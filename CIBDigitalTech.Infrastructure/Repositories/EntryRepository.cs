﻿using CIBDigitalTech.Core.Dtos;
using CIBDigitalTech.Core.Dtos.Response.GatewayResponses.Repositories;
using CIBDigitalTech.Core.Entities;
using CIBDigitalTech.Core.Interfaces.Gateways.Repositories;
using CIBDigitalTech.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CIBDigitalTech.Infrastructure.Repositories
{
    public class EntryRepository : EfRepository<Entry>, IEntryRepository
    {
        public EntryRepository(AppDbContext appDbContext) : base(appDbContext)
        {

        }

        public async Task<Response> Create(string name, string phoneNumber, Guid phoneBookId)
        {
            if (_appDbContext.Entries.Any(x => x.Name == name))
                throw new Exception("Entry \"" + name + "\" is already taken");
            var entry = new Entry(name, phoneNumber, phoneBookId);
            await Add(entry);
            return new Response("Phonebook Entry Created successfully", true);
           
        }
    }
}
