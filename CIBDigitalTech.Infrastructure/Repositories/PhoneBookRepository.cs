﻿using CIBDigitalTech.Core.Dtos;
using CIBDigitalTech.Core.Dtos.Response.GatewayResponses.Repositories;
using CIBDigitalTech.Core.Dtos.Responses.PhoneBook;
using CIBDigitalTech.Core.Entities;
using CIBDigitalTech.Core.Interfaces.Gateways.Repositories;
using CIBDigitalTech.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CIBDigitalTech.Infrastructure.Repositories
{
    public class PhoneBookRepository : EfRepository<PhoneBook>, IPhoneBookRepository
    {
        public PhoneBookRepository(AppDbContext appDbContext) : base(appDbContext)
        {

        }

        public async Task<Response> Create(string name)
        {
            if (_appDbContext.PhoneBooks.Any(x => x.Name == name))
                throw new Exception("Phonebook \"" + name + "\" is already taken");
            var phoneBook = new PhoneBook(name);
            await Add(phoneBook);
            return new Response("Phonebook created successfully", true);
          
        }
        public async Task<IEnumerable<PhoneBookDto>> GetPhoneBooks()
        {
            var phoneBooks = from p in _appDbContext.PhoneBooks
                             select new PhoneBookDto
                             {
                                 Id = p.Id,
                                 PhoneBookName = p.Name,
                                 Entry = p.Entries.ToList()
                             };
            return await phoneBooks.ToListAsync();
        }

        public async Task<IEnumerable<PhoneBookDto>> GetPhoneBookEntryByName(Guid phoneBookId, string name)
        {
            var phoneBooks = from p in _appDbContext.PhoneBooks
                             where p.Id == phoneBookId
                             select new PhoneBookDto
                             {
                                 Id = p.Id,
                                 PhoneBookName = p.Name,
                                 Entry = p.Entries.Where(x => x.Name == name).ToList()
                             };
            return await phoneBooks.ToListAsync();
        }
    }
}
