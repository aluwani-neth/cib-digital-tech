﻿using Autofac;
using CIBDigitalTech.Core.Interfaces.Gateways.Repositories;
using CIBDigitalTech.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace CIBDigitalTech.Infrastructure
{
    public class InfrastructureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PhoneBookRepository>().As<IPhoneBookRepository>().InstancePerLifetimeScope();
            builder.RegisterType<EntryRepository>().As<IEntryRepository>().InstancePerLifetimeScope();
        }
    }
}
